# FaceRecognition #
This project was written in Python 3.7.7

## Development server
What you need step by step:
 
 *  PyCharm: https://www.jetbrains.com/pycharm/download/
 *  Python 3.7.7 64 bit: https://www.python.org/downloads/release/python-377/
 *  Nvidia CUDA 10.2: https://developer.nvidia.com/cuda-downloads **or** AMD: https://bit.ly/3aNsbvl
 *  Flask: pip install Flask
 *  Flask-RESTful: pip install flask-restful
 *  Flask-cors: pip install -U flask-cors
 *  Pillow: pip install Pillow
 *  Facenet-pytorch: pip install facenet-pytorch
 *  PyTorch: https://pytorch.org/
 *  OpenCV on Wheels: pip install opencv-python
 *  open project folder with PyCharm
 *  run FaceRecognitionController.py

## Endpoints
http://localhost:8090/result

POST method with JSON request body containing two keys:

 *  image (string) - image converted to base64
 *  numberOfResults (number) - number of pairs (url, percentage similarity) requested

       
returns uniqueId which should be used in GET method to get results. 
       
example body:
    
```json
{
	"image":"/9j/4AAQSkZJRgABAQEASABIAAD/4gIcSUNDX1BST0ZJTEUAAQEAAAIMbGNtc...",
	"numberOfResults":10
}
```

example return:
```json
{
    "id": "bf360d00-8315-11ea-a5b9-58a023c00737",
    "number": 10,
    "image":"/9j/4AAQSkZJRgABAQEASABIAAD/4gIcSUNDX1BST0ZJTEUAAQEAAAIMbGNtc..."
}
```
  
GET method with one attribute in the URL:

 *  uniqueId (string) - unique ID which is using to get results 

       
       returns list of pairs (url, percentage similarity) 
       
example URL:
```
http://localhost:8090/result/bf360d00-8315-11ea-a5b9-58a023c00737
```

example return:
```json
[
    {
        "url": "http://156.17.128.143/00e82c24-af1e-11ea-8441-58a023c00737.jpg",
        "listOfResults": [
            {
                "percentage": 38.77124631343765,
                "url": "http://156.17.128.143/data/img_align_celeba_out/137509.jpg"
            },
            {
                "percentage": 34.49882393828449,
                "url": "http://156.17.128.143/data/img_align_celeba_out/178205.jpg"
            },
            {
                "percentage": 33.400497690527274,
                "url": "http://156.17.128.143/data/img_align_celeba_out/011895.jpg"
            },
            {
                "percentage": 31.737349455672437,
                "url": "http://156.17.128.143/data/img_align_celeba_out/171562.jpg"
            },
            {
                "percentage": 29.94376244701007,
                "url": "http://156.17.128.143/data/img_align_celeba_out/171139.jpg"
            }
        ]
    }
]
```
There is a result is in the url.



