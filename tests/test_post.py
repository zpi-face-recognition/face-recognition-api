import unittest
import json
from app.controller.FaceRecognitionController import FaceRecognitionController


class PostTest(unittest.TestCase):

    def setUp(self):
        self.app = FaceRecognitionController().app.test_client()

    def test_successful_post(self):
        # Given
        payload = json.dumps({
            "url": "https://engineering.unl.edu/images/staff/Kayla_Person-small.jpg",
            "numberOfResults": 10
        })

        # When
        response = self.app.post('/result/', headers={"Content-Type": "application/json"}, data=payload)

        # Then
        self.assertEqual(str, type(response.json['id']))
        self.assertEqual(10, response.json['number'])
        self.assertEqual("https://engineering.unl.edu/images/staff/Kayla_Person-small.jpg", response.json['url'])
        self.assertEqual(200, response.status_code)

    def test_unsuccessful_post_wrong_url(self):
        # Given
        payload = json.dumps({
            "url": "hktps://engineering.unl.edu/images/staff/Kayla_Person-small.jpg",
            "numberOfResults": 10
        })

        # When
        response = self.app.post('/result/', headers={"Content-Type": "application/json"}, data=payload)

        # Then
        self.assertEqual("Invalid format of URL", response.json['message'])
        self.assertEqual(400, response.status_code)

    def test_unsuccessful_post_missing_attribute_numberOfResults(self):
        # Given
        payload = json.dumps({
            "url": "https://engineering.unl.edu/images/staff/Kayla_Person-small.jpg",
        })

        # When
        response = self.app.post('/result/', headers={"Content-Type": "application/json"}, data=payload)

        # Then
        self.assertEqual("Request is missing required fields", response.json['message'])
        self.assertEqual(400, response.status_code)

    def test_unsuccessful_post_missing_attribute_url(self):
        # Given
        payload = json.dumps({
            "numberOfResults": 10,
        })

        # When
        response = self.app.post('/result/', headers={"Content-Type": "application/json"}, data=payload)

        # Then
        self.assertEqual("Request is missing required fields", response.json['message'])
        self.assertEqual(400, response.status_code)

    def tearDown(self):
        pass
