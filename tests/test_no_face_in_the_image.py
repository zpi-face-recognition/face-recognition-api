import unittest
import json
from app.controller.FaceRecognitionController import FaceRecognitionController


class TestNoFace(unittest.TestCase):

    def setUp(self):
        self.app = FaceRecognitionController().app.test_client()

    def test_method(self):
        # Given
        payload = json.dumps({
            "url": "https://www.tubadzin.pl/sites/default/files/pl_PS-Colour-Black.jpg",
            "numberOfResults": 10
        })

        # When
        response_post = self.app.post('/result/', headers={"Content-Type": "application/json"}, data=payload)
        generated_id = response_post.json['id']
        response_get = self.app.get('/result/' + generated_id)

        # Then
        self.assertEqual("No face detected", response_get.json['message'])
        self.assertEqual(200, response_get.status_code)

    def tearDown(self):
        pass
