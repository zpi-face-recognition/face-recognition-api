import unittest
import json
from app.controller.FaceRecognitionController import FaceRecognitionController


class BaseCase(unittest.TestCase):

    def setUp(self):
        self.app = FaceRecognitionController().app.test_client()

    def test_method(self):
        # Given
        payload = json.dumps({
            "url": "https://engineering.unl.edu/images/staff/Kayla_Person-small.jpg",
            "numberOfResults": 10
        })

        # When
        response = self.app.post('/result/', headers={"Content-Type": "application/json"}, data=payload)

        # Then
        self.assertEqual(str, type(response.json['id']))
        self.assertEqual(10, response.json['number'])
        self.assertEqual("https://engineering.unl.edu/images/staff/Kayla_Person-small.jpg", response.json['url'])
        self.assertEqual(200, response.status_code)

    def tearDown(self):
        pass
