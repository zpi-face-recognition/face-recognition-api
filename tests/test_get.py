import unittest
import json
from app.controller.FaceRecognitionController import FaceRecognitionController


class GetTest(unittest.TestCase):

    def setUp(self):
        self.app = FaceRecognitionController().app.test_client()

    def test_successful_get(self):
        # Given
        payload = json.dumps({
            "url": "https://www.thestatesman.com/wp-content/uploads/2017/08/1493458748-beauty-face-517.jpg",
            "numberOfResults": 10
        })

        # When
        response_post = self.app.post('/result/', headers={"Content-Type": "application/json"}, data=payload)
        generated_id = response_post.json['id']
        response_get = self.app.get('/result/'+generated_id)

        # Then
        response_json = response_get.json

        self.assertEqual(list, type(response_json))

        face_result = response_json[0]
        self.assertEqual(list, type(face_result["listOfResults"]))
        self.assertEqual(str, type(face_result["url"]))

        similar_face_result = face_result["listOfResults"][0]
        self.assertEqual(float, type(similar_face_result["percentage"]))
        self.assertEqual(str, type(similar_face_result["url"]))

        self.assertEqual(200, response_get.status_code)

    def test_unsuccessful_get_wrong_id(self):
        # Given
        payload = json.dumps({
            "url": "https://engineering.unl.edu/images/staff/Kayla_Person-small.jpg",
            "numberOfResults": 10
        })

        # When
        response_post = self.app.post('/result/', headers={"Content-Type": "application/json"}, data=payload)
        generated_id = response_post.json['id']
        response_get = self.app.get('/result/' + generated_id+"some_wrong_characters")

        # Then
        self.assertEqual("This request id doesn't exist on the server", response_get.json['message'])
        self.assertEqual(404, response_get.status_code)


    def tearDown(self):
        pass
