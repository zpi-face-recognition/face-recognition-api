from app.face_detection.FaceDetectionMIX import FaceDetectionMix
from app.face_detection.FaceDetection import FaceDetection
from app.face_detection.FaceDetectionOpenCV import FaceDetectionOpenCV
import cv2
import os
import time

not_detected = "not_detected/"
to_many_detected = "to_many_detected/"
txt_output = "errors.txt"
data_path = "D:/Projekty/TEST/FaceDetection_DataMultiple/"
output_NN = "D:/Projekty/TEST/FaceDetectionResult_NN_m/"
output_Mix = "D:/Projekty/TEST/FaceDetectionResult_MIX_m/"
output_OpenCV = "D:/Projekty/TEST/FaceDetectionResult_OpenCV_m/"

face_detection_NN = FaceDetection(0, (128, 128))
face_detection_OpenCV = FaceDetectionOpenCV(0, (128, 128))
face_detection_Mix = FaceDetectionMix(0, (128, 128))

i = 1
number_of_tests = 754
initial_number_of_test = number_of_tests
wykryto_poprawnie = 0
wykryto_zaduzo = 0
nie_wykryto = 0
txt = ""

# Neural Network face detection
start_time = time.time()
for photo in os.listdir(data_path):
    if number_of_tests > 0:
        try:
            photo = data_path + photo
            img = cv2.imread(photo, cv2.COLOR_BGR2RGB)
            try:
                result_NN = face_detection_NN.detect_face(img)
                if len(result_NN) == 0:
                    nie_wykryto += 1
                    name = str(i) + ".jpg"
                    cv2.imwrite(os.path.join(output_NN + not_detected, name), img)
                    i += 1
                else:
                    for cropped in result_NN:
                        wykryto_poprawnie += 1
                        name = str(i) + ".jpg"
                        cv2.imwrite(os.path.join(output_NN, name), cropped)
                        i += 1
            except:
                nie_wykryto += 1
                img = cv2.imread(photo, cv2.IMREAD_UNCHANGED)
                name = str(i) + ".jpg"
                cv2.imwrite(os.path.join(output_NN + not_detected, name), img)
                txt += str(photo) + " <-niewykryto\n"
        except Exception as e:
            txt += str(photo) + "<- Nie wyczytano\n"
        number_of_tests -= 1
    else:
        break

endtime = time.time() - start_time
print("NN time:", endtime)
txt = "Czas działania: " + str(endtime) + "\n" + txt
txt = "Wykryto zaduzo: " + str(wykryto_zaduzo) + "\n" + txt
txt = "Nie wykryto: " + str(nie_wykryto) + "\n" + txt
txt = "Wykryto poprawnie: " + str(wykryto_poprawnie) + "\n" + txt
txt = "Liczba zdjęć : " + str(initial_number_of_test) + "\n" + txt
txt = "Metoda NN\n" + txt
text_file = open(output_NN + txt_output, "w")
text_file.write(txt)
text_file.close()

"""
# Mixed face detection
i = 1
number_of_tests = 100
initial_number_of_test = number_of_tests
wykryto_poprawnie = 0
wykryto_zaduzo = 0
nie_wykryto = 0
txt = ""

start_time = time.time()
for photo in os.listdir(data_path):
    if number_of_tests > 0:
        try:
            photo = data_path + photo
            img = cv2.imread(photo, cv2.IMREAD_UNCHANGED)
            try:
                result_Mix = face_detection_Mix.detect_face(img)

                if len(result_Mix) == 0:
                    nie_wykryto += 1
                    name = str(i) + ".jpg"
                    cv2.imwrite(os.path.join(output_Mix + not_detected, name), img)
                    i += 1
                else:
                    for cropped in result_Mix:
                        wykryto_poprawnie += 1
                        name = str(i) + ".jpg"
                        cv2.imwrite(os.path.join(output_Mix, name), cropped)
                        i += 1
            except:
                nie_wykryto += 1
                img = cv2.imread(photo, cv2.IMREAD_UNCHANGED)
                name = str(i) + ".jpg"
                cv2.imwrite(os.path.join(output_Mix + not_detected, name), img)
                txt += str(photo) + " <-niewykryto\n"
        except Exception as e:
            txt += str(photo) + "<- Nie wyczytano\n"
        number_of_tests -= 1
    else:
        break

endtime = time.time() - start_time
print("Mixed time:", endtime)
txt = "Czas działania: " + str(endtime) + "\n" + txt
txt = "Wykryto zaduzo: " + str(wykryto_zaduzo) + "\n" + txt
txt = "Nie wykryto: " + str(nie_wykryto) + "\n" + txt
txt = "Wykryto poprawnie: " + str(wykryto_poprawnie) + "\n" + txt
txt = "Liczba zdjęć : " + str(initial_number_of_test) + "\n" + txt
txt = "Metoda Mixed\n" + txt
text_file = open(output_Mix + txt_output, "w")
text_file.write(txt)
text_file.close()

# OpenCV face detection

i = 1
number_of_tests = 754
initial_number_of_test = number_of_tests
wykryto_poprawnie = 0
wykryto_zaduzo = 0
nie_wykryto = 0
txt = ""

start_time = time.time()
for photo in os.listdir(data_path):
    if number_of_tests > 0:
        print(photo)
        try:
            path = data_path + photo
            img = cv2.imread(path, cv2.IMREAD_UNCHANGED)
            try:
                result = face_detection_OpenCV.detect_face(img)
                if len(result) == 0:
                    nie_wykryto += 1
                    txt += str(path) + " <-niewykryto\n"
                    name =photo + "_" +  str(i)  + ".jpg"
                    cv2.imwrite(os.path.join(output_OpenCV + not_detected, name), img)
                else:
                    for cropped in result:
                        wykryto_poprawnie += 1
                        name =photo + "_" +  str(i)  + ".jpg"
                        cv2.imwrite(os.path.join(output_OpenCV, name), cropped)
                        i += 1
            except:
                nie_wykryto += 1
                img = cv2.imread(photo, cv2.IMREAD_UNCHANGED)
                name =photo + "_" +  str(i)  + ".jpg"
                cv2.imwrite(os.path.join(output_OpenCV + not_detected, name), img)
                txt += str(path) + " <-niewykryto\n"
        except Exception as e:
            txt += str(path) + "<- Nie wyczytano\n"
        number_of_tests -= 1
        i = 1
    else:
        break

endtime = time.time() - start_time
print("OpenCV time:", endtime)
txt = "Czas działania: " + str(endtime) + "\n" + txt
txt = "Wykryto zaduzo: " + str(wykryto_zaduzo) + "\n" + txt
txt = "Nie wykryto: " + str(nie_wykryto) + "\n" + txt
txt = "Wykryto poprawnie: " + str(wykryto_poprawnie) + "\n" + txt
txt = "Liczba zdjęć : " + str(initial_number_of_test) + "\n" + txt
txt = "Metoda OpenCV\n" + txt
text_file = open(output_OpenCV + txt_output, "w")
text_file.write(txt)
text_file.close()
"""