from flask import Flask, request, jsonify
from app.service.FaceRecognitionService import *
from flask_restful import Resource, Api, reqparse
from app.Constants import *
from flask_cors import CORS
from app.regex import *
import sys
from app.errors import errors, SchemaValidationError, InvalidURLFormatError


class FaceRecognitionController:

    def __init__(self):
        self.app = Flask(__name__)
        CORS(self.app)
        self.api = Api(self.app, errors=errors)
        self.api.add_resource(self.GetResults, '/result', '/result/', '/result/<uniqueId>')

    class GetResults(Resource):

        service = FaceRecognitionService()
        if not isinstance(service, IFaceRecognitionService):
            raise Exception

        def post(self):
            some_json = request.get_json()
            try:
                number_of_results = int(some_json['numberOfResults'])
                image_base64 = some_json['image']
                new_request_json = self.service.save_request(number_of_results, image_base64)
                return new_request_json

            except KeyError:
                raise SchemaValidationError
            except InvalidURLFormatError:
                raise InvalidURLFormatError

        def get(self, uniqueId):
            result_json = self.service.run_request(uniqueId)
            return result_json

    def run(self):
        self.app.run(port=PORT(),host='0.0.0.0')


if __name__ == '__main__':
    controller = FaceRecognitionController()
    controller.run()
