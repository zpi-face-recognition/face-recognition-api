from app.face_detection.FaceDetectionMIX import FaceDetectionMix
from app.face_detection.FaceDetection import FaceDetection
from app.face_detection.FaceDetectionOpenCV import FaceDetectionOpenCV
import cv2
import os
import time

not_detected = "not_detected/"
to_many_detected = "to_many_detected/"
txt_output = "errors.txt"
data_path = "D:/Projekty/TEST/FaceDetection_DATA/"
output_NN = "D:/Projekty/TEST/FaceDetectionResult_NN/"
output_Mix = "D:/Projekty/TEST/FaceDetectionResult_MIX/"
output_OpenCV = "D:/Projekty/TEST/FaceDetectionResult_OpenCV/"

face_detection_NN = FaceDetection(0, (128, 128))
face_detection_OpenCV = FaceDetectionOpenCV(0, (128, 128))
face_detection_Mix = FaceDetectionMix(30, (128, 128))


i = 1
number_of_tests = 100
initial_number_of_test = number_of_tests
wykryto_poprawnie = 0
wykryto_zaduzo = 0
nie_wykryto = 0
txt = ""

# Neural Network face detection
start_time = time.time()
for photo in os.listdir(data_path):
    if number_of_tests > 0:
        try:
            photo = data_path + photo
            img = cv2.imread(photo, cv2.IMREAD_UNCHANGED)
            try:
                result_NN = face_detection_NN.detect_face(img)
                if len(result_NN) > 1:
                    wykryto_zaduzo += 1
                    txt += str(photo) + " <- zbyt wiele twarzy\n"
                    for cropped in result_NN:
                        name = str(i) + ".jpg"
                        cv2.imwrite(os.path.join(output_NN + to_many_detected, name), cropped)
                        i += 1
                else:
                    if len(result_NN) == 0:
                        nie_wykryto += 1
                        name = str(i) + ".jpg"
                        cv2.imwrite(os.path.join(output_NN + not_detected, name), img)
                        i += 1
                    else:
                        for cropped in result_NN:
                            wykryto_poprawnie += 1
                            name = str(i) + ".jpg"
                            cv2.imwrite(os.path.join(output_NN, name), cropped)
                            i += 1
            except:
                nie_wykryto += 1
                img = cv2.imread(photo, cv2.IMREAD_UNCHANGED)
                name = str(i) + ".jpg"
                cv2.imwrite(os.path.join(output_NN + not_detected, name), img)
                txt += str(photo) + " <-niewykryto\n"
        except Exception as e:
            txt += str(photo) + "<- Nie wyczytano\n"
        number_of_tests -= 1
    else:
        break

endtime = time.time() - start_time
print("NN time:", endtime)
txt = "Czas działania: " + str(endtime) + "\n" + txt
txt = "Wykryto zaduzo: " + str(wykryto_zaduzo) + "\n" + txt
txt = "Nie wykryto: " + str(nie_wykryto) + "\n" + txt
txt = "Wykryto poprawnie: " + str(wykryto_poprawnie) + "\n" + txt
txt = "Liczba zdjęć : " + str(initial_number_of_test) + "\n" + txt
txt = "Metoda NN\n" + txt
text_file = open(output_NN + txt_output, "w")
text_file.write(txt)
text_file.close()


# Mixed face detection
i = 1
number_of_tests = 100
initial_number_of_test = number_of_tests
wykryto_poprawnie = 0
wykryto_zaduzo = 0
nie_wykryto = 0
txt = ""


start_time = time.time()
for photo in os.listdir(data_path):
    if number_of_tests > 0:
        try:
            photo = data_path + photo
            img = cv2.imread(photo, cv2.IMREAD_UNCHANGED)
            try:
                result_Mix = face_detection_Mix.detect_face(img)
                if len(result_Mix) > 1:
                    wykryto_zaduzo += 1
                    txt += str(photo) + " <- zbyt wiele twarzy\n"
                    for cropped in result_Mix:
                        name = str(i) + ".jpg"
                        cv2.imwrite(os.path.join(output_Mix + to_many_detected, name), cropped)
                        i += 1
                else:
                    if len(result_Mix) == 0:
                        nie_wykryto += 1
                        name = str(i) + ".jpg"
                        cv2.imwrite(os.path.join(output_Mix + not_detected, name), img)
                        i += 1
                    else:
                        for cropped in result_Mix:
                            wykryto_poprawnie += 1
                            name = str(i) + ".jpg"
                            cv2.imwrite(os.path.join(output_Mix, name), cropped)
                            i += 1
            except:
                nie_wykryto += 1
                img = cv2.imread(photo, cv2.IMREAD_UNCHANGED)
                name = str(i) + ".jpg"
                cv2.imwrite(os.path.join(output_Mix + not_detected, name), img)
                txt += str(photo) + " <-niewykryto\n"
        except Exception as e:
            txt += str(photo) + "<- Nie wyczytano\n"
        number_of_tests -= 1
    else:
        break

endtime = time.time() - start_time
print("Mixed time:", endtime)
txt = "Czas działania: " + str(endtime) + "\n" + txt
txt = "Wykryto zaduzo: " + str(wykryto_zaduzo) + "\n" + txt
txt = "Nie wykryto: " + str(nie_wykryto) + "\n" + txt
txt = "Wykryto poprawnie: " + str(wykryto_poprawnie) + "\n" + txt
txt = "Liczba zdjęć : " + str(initial_number_of_test) + "\n" + txt
txt = "Metoda Mixed\n" + txt
text_file = open(output_Mix + txt_output, "w")
text_file.write(txt)
text_file.close()


# OpenCV face detection

i = 1
number_of_tests = 100
initial_number_of_test = number_of_tests
wykryto_poprawnie = 0
wykryto_zaduzo = 0
nie_wykryto = 0
txt = ""


start_time = time.time()
for photo in os.listdir(data_path):
    if number_of_tests > 0:
        try:
            photo = data_path + photo
            img = cv2.imread(photo, cv2.IMREAD_UNCHANGED)
            try:
                result = face_detection_OpenCV.detect_face(img)
                if len(result) > 1:
                    wykryto_zaduzo += 1
                    txt += str(photo) + " <- zbyt wiele twarzy\n"
                    for cropped in result:
                        name = str(i) + ".jpg"
                        cv2.imwrite(os.path.join(output_OpenCV + to_many_detected, name), cropped)
                        i += 1
                else:
                    if len(result) == 0:
                        nie_wykryto += 1
                        txt += str(photo) + " <-niewykryto\n"
                        name = str(i) + ".jpg"
                        cv2.imwrite(os.path.join(output_OpenCV + not_detected, name), img)
                        i += 1
                    else:
                        for cropped in result:
                            wykryto_poprawnie += 1
                            name = str(i) + ".jpg"
                            cv2.imwrite(os.path.join(output_OpenCV, name), cropped)
                            i += 1
            except:
                nie_wykryto += 1
                img = cv2.imread(photo, cv2.IMREAD_UNCHANGED)
                name = str(i) + ".jpg"
                cv2.imwrite(os.path.join(output_OpenCV + not_detected, name), img)
                txt += str(photo) + " <-niewykryto\n"
        except Exception as e:
            txt += str(photo) + "<- Nie wyczytano\n"
        number_of_tests -= 1
    else:
        break

endtime = time.time() - start_time
print("OpenCV time:", endtime)
txt = "Czas działania: " + str(endtime) + "\n" + txt
txt = "Wykryto zaduzo: " + str(wykryto_zaduzo) + "\n" + txt
txt = "Nie wykryto: " + str(nie_wykryto) + "\n" + txt
txt = "Wykryto poprawnie: " + str(wykryto_poprawnie) + "\n" + txt
txt = "Liczba zdjęć : " + str(initial_number_of_test) + "\n" + txt
txt = "Metoda OpenCV\n" + txt
text_file = open(output_OpenCV + txt_output, "w")
text_file.write(txt)
text_file.close()

