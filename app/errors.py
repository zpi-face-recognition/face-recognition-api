class InternalServerError(Exception):
    pass


class SchemaValidationError(Exception):
    pass


class InvalidURLFormatError(Exception):
    pass


class FaceRecognitionError(Exception):
    pass


class UploadImageError(Exception):
    pass


class RequestIdNotFoundError(Exception):
    pass


class NoFaceDetected(Exception):
    pass


errors = {
    "InternalServerError": {
        "message": "Something went wrong",
        "status": 500
    },
    "FaceRecognitionError": {
        "message": "Something went wrong with face recognition",
        "status": 500
    },
    "UploadImageError": {
        "message": "There was a problem uploading the image",
        "status": 500
    },
    "SchemaValidationError": {
        "message": "Request is missing required fields",
        "status": 400
    },
    "InvalidURLFormatError": {
        "message": "Invalid format of URL",
        "status": 400
    },
    "RequestIdNotFoundError": {
        "message": "This request id doesn't exist on the server",
        "status": 404
    },
    "NoFaceDetected": {
        "message": "No face detected",
        "status": 406
    },
}
