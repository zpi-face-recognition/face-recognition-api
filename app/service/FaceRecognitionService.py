import base64
import ftplib
import os

from app.errors import RequestIdNotFoundError, UploadImageError, NoFaceDetected
from app.model.Request import Request
from app.service.image_service.ImageFromFTP import ImageFromFTP

from app.service.image_service.ImageFromUrl import ImageFromUrl
from app.service.IFaceRecognitionService import *
from app.service.image_service.UploadImage import UploadImage
from app.face_detection.FaceDetectionOpenCV import FaceDetectionOpenCV
from app.face_detection.FaceDetection import FaceDetection
from app.service.image_service.ImageFromUrlOpenCV import ImageFromUrlOpenCV
from app.model.FaceResult import FaceResult
from app.face_recognition.FaceRecognition import FaceRecognition
import cv2
import uuid
import json
from urllib.parse import urlparse

from app.service.image_service.UploadImageFTP import UploadImageFTP


class FaceRecognitionService(IFaceRecognitionService):

    def __init__(self):
        self.list_of_requests = []
        self.face_detection_algorithm = FaceDetectionOpenCV(padding=0, image_size=(128, 128))
        # self.face_detection_algorithm = FaceDetection()
        # self.face_detection_algorithm = FaceDetectionFR(padding=0, image_size=(128, 128))
        self.recognition_algorithm = FaceRecognition()

    def save_request(self, number_of_result, image_base64):

        unique_id = uuid.uuid1()  # generowanie unikalnego ID

        new_request = Request(unique_id, number_of_result, image_base64)

        # Zapisywanie requesta do listy.
        self.list_of_requests.append(new_request)

        new_request_json = json.dumps(new_request.__dict__)  # ta linia konwertuje objekt Request w strukture JSON
        new_request_json = json.loads(new_request_json)  # dzięki tej lini JSON wygląda lepiej
        return new_request_json


    def run_request(self, unique_id):
        request_to_run = self.find_request(unique_id)
        result = []
        if request_to_run is None:
            raise RequestIdNotFoundError

        # nawiazanie polaczenia z FTP
        session = ftplib.FTP('156.17.128.143')
        session.login()

        #pobrane typu protokolu z url i
        #Pobieranie zdjecia z serwera ftp lub http
        # protocol = urlparse(request_to_run.url).scheme
        # if protocol == 'ftp':
        #     img = ImageFromFTP.getImage(request_to_run.url, session)
        # else:
        #     img = ImageFromUrlOpenCV.getImage(request_to_run.url)

        #Dekodowanie zdjecia z formatu base64 w Requescie
        filename = unique_id+".jpg"
        imgdata = base64.b64decode(request_to_run.image)
        with open(filename, 'wb') as f:
            f.write(imgdata)
        img = cv2.imread(filename)
        os.remove(filename)

        res = self.face_detection_algorithm.detect_face(img)
        recognized = self.recognition_algorithm.recognise_faces(res, request_to_run.number)

        if len(recognized) == 0:
            raise NoFaceDetected

        urls = []

        for face in res:
            url = UploadImageFTP.uploadImage(face, session) #uploadowanie na ftp
            # url = UploadImage.uploadImage(face) # uploadowanie na imgura
            urls.append(url)

        i = 0

        # Gdy wyniki wskazywane na FTP/HTTP
        for rec in recognized:
            result.append(FaceResult(urls[i], rec))
            i += 1

        # Gdy wyniki uploadowane na imgura
        # for rec in recognized:
        #     for res_in_list in rec:
        #         # local_url = "D:/ZPI-FaceRecognition/data" + urlparse(res_in_list.url).path
        #         # local_url = local_url.replace('/', '\\')
        #         # print(local_url)
        #         # img = cv2.imread(local_url)
        #         img = ImageFromFTP.getImage(res_in_list.url, session)
        #         url = UploadImage.uploadImage(img)
        #         res_in_list.url = url
        #     result.append(FaceResult(urls[i], rec))
        #     i += 1

        # zamkniecie polaczenia z FTP
        session.close()
        # except:
        #     print("Unexpected error:", sys.exc_info()[0])
        #     return 400


        json_string = json.dumps(result, default=lambda
            obj: obj.__dict__)  # ta linia konwertuje nasze obiekty Result w strukture JSON
        final_dictionary = json.loads(json_string)
        return final_dictionary

    def find_request(self, unique_id):
        for req in self.list_of_requests:
            if str(req.id) == str(unique_id):
                return req
        return None
