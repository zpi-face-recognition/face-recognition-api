from abc import ABC, abstractmethod


class IFaceRecognitionService(ABC):

    @abstractmethod
    def save_request(self, number_of_result, image_base64):
        pass

    @abstractmethod
    def run_request(self, unique_id):
        pass

    @abstractmethod
    def find_request(self, unique_id):
        pass
