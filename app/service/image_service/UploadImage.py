import json
from io import BytesIO
import requests
import base64
from PIL import Image
import cv2

from app.errors import UploadImageError


class UploadImage:

    # defining the api-endpoint
    API_ENDPOINT = "https://api.imgur.com/3/upload"
    header = {'Authorization': 'Client-ID 0bcc78aa7144e00'}

    @staticmethod
    def uploadImage(image):

        try:
            retval, buffer_img = cv2.imencode('.jpg', image)
            data = base64.b64encode(buffer_img)

            data = {'image': data,
                    'type': 'base64'}

            r = requests.post(url=UploadImage.API_ENDPOINT, data=data, headers=UploadImage.header)

            if r.status_code == 200:
                json_data = json.loads(r.text)
                link = json_data['data']['link']
            else:
                raise UploadImageError

            return link
        except:
            raise UploadImageError
