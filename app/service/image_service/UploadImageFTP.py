import base64
import ftplib
import json
import sys
import uuid

import cv2
import requests

from app.errors import UploadImageError
import os

class UploadImageFTP:
    ftp_address = 'ftp://156.17.128.143/'
    http_address = 'http://156.17.128.143/'

    @staticmethod
    def uploadImage(image, session):
        try:

            filename = uuid.uuid1()  # generowanie unikalnego ID
            filename = str(filename) + '.jpg'

            cv2.imwrite(filename, image)
            #
            # retval, buffer_img = cv2.imencode('.jpg', image)
            # data = base64.b64encode(buffer_img)
            #
            # filename = uuid.uuid1()  # generowanie unikalnego ID
            # filename = str(filename) + '.jpg'
            # new_file = open(filename, 'wb')
            # new_file.write(data)
            # new_file.close()

            file = open(filename, 'rb')
            session.storbinary('STOR %s' % filename, file)
            file.close()

            os.remove(filename)

            # link = UploadImageFTP.ftp_address + filename
            link = UploadImageFTP.http_address + filename

            return link
        except Exception:
            print(sys.exc_info()[0])
            raise UploadImageError
