import os
import sys
from ftplib import FTP
from urllib.parse import urlparse

import cv2
import urllib

import numpy as np
import requests


class ImageFromFTP:
    ftp_address = '156.17.128.143'

    @staticmethod
    def getImage(url, session):
        try:
            filename = url.rsplit('/', 1)[-1]

            #wycinanie z url czesci path
            path = urlparse(url).path
            path = path.rsplit('/',1)[0]

            #zapamietywanie domyslnej path
            default_directory = session.pwd()

            session.cwd(path)
            session.retrbinary("RETR " + filename, open(filename, 'wb').write)
            session.cwd(default_directory)

            image = cv2.imread(filename)

            os.remove(filename)

            # response = requests.get(url, stream=True)
            # resp = response.raw
            #
            # image = np.asarray(bytearray(resp.read()), dtype="uint8")
            # image = cv2.imdecode(image, cv2.IMREAD_UNCHANGED)

        except Exception:
            print(sys.exc_info()[0])
            raise Exception
        return image
