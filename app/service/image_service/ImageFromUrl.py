import requests
import cv2
import numpy as np
from PIL import  Image
class ImageFromUrl:

    @staticmethod
    def getImage(url):
        try:
            response = requests.get(url, stream=True)
            image = Image.open(response.raw)
        except Exception:
            raise Exception
        return image
