from io import BytesIO
import requests
import cv2
import numpy as np


class ImageFromUrlOpenCV:

    @staticmethod
    def getImage(url):
        try:
            response = requests.get(url, stream=True)
            resp = response.raw

            image = np.asarray(bytearray(resp.read()), dtype="uint8")
            image = cv2.imdecode(image, cv2.IMREAD_UNCHANGED)

        except Exception:
            raise Exception
        return image
