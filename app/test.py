import ftplib
import time
from functools import partial
from io import BytesIO

import cv2
import numpy as np
import os
import multiprocessing
import face_recognition
import requests
from PIL import Image


p = 0
faces = []
# inputdir = "D:/Projekty/Similar faces/"
# outputdir = "D:/Projekty/Similar faces out/"
inputdir = "D:/ZPI-FaceRecognition/data/data/Zdj przerobki/"
outputdir = "D:/ZPI-FaceRecognition/data/data/Zdj przerobki out/"
outputdirfaces2 = "D:/ZPI-FaceRecognition/data/Zdj przerobki out/"
inputputdirfaces2 = "D:/ZPI-FaceRecognition/data/Zdj przerobki/"
# innerpath = "data/img_align_celeba_out/"
innerpath = "data/Zdj przerobki out/"


# Detection
# face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
#
# for fichier in os.listdir(inputdir):
#     img = cv2.imread(inputdir+fichier, cv2.IMREAD_COLOR)
#     print(type(img))
#     print(np.shape(img))
#     print(img)
#
#     faces_detected = face_cascade.detectMultiScale(img, scaleFactor=1.1, minNeighbors=5)
#     # Preprocessing
#     for face in faces_detected:
#         (x, y, w, h) = face
#         cropped = img[y-p+1:y+h+p, x-p+1:x+w+p]
#         resized = cv2.resize(cropped, (128, 128), interpolation=cv2.INTER_LINEAR)
#         norm_img = np.zeros((128, 128))
#         norm_img = cv2.normalize(resized, norm_img, 0, 255, cv2.NORM_MINMAX)
#         cv2.imwrite(outputdir+fichier, norm_img)

# Recognition



def process_file(args):
    try:
        img = face_recognition.load_image_file("D:/ZPI-FaceRecognition/data/" + args[0])
        encoding = face_recognition.face_encodings(img)[0]
        print(np.shape(encoding))
        args[1].append((args[0], encoding))
        print(args[0])
    except:
        print(args[0] + " <- error")


def pool_handler():
    full_path = []
    manager = multiprocessing.Manager()
    encodings = manager.list()
    for face in os.listdir(outputdir):
        # full_path.append((outputdir + face, encodings))
        full_path.append((innerpath + face, encodings))

    p = multiprocessing.Pool(10)
    p.map(process_file, full_path)
    return encodings


if __name__ == '__main__':
    start_time = time.time()
    enc = pool_handler()
    np.save("face_encodings_similar_4", enc)
    with open("face_encodings_similar_3.npy", "ab") as myfile, open("face_encodings_similar_4.npy", "rb") as file2:
        myfile.write(file2.read())
    print("--- %s seconds ---" % (time.time() - start_time))

#matrix = np.load("face_encodings_similar_4.npy", allow_pickle=True)
#print(matrix)
"""
url = "https://upload.wikimedia.org/wikipedia/commons/5/5e/Happy_family_%281%29.jpg"

response = requests.get(url, stream=True)
resp = response.raw


image = np.asarray(bytearray(resp.read()), dtype="uint8")
image = cv2.imdecode(image, cv2.IMREAD_UNCHANGED)


cv2.imshow('okienko',image)
cv2.waitKey(0)

response = requests.get(url)
im = Image.open(BytesIO(response.content))
Image._show(im)


open_cv_image = np.array(image)
cv2.imshow('okienko',open_cv_image)
cv2.waitKey(0)




# Displaying
#for face in faces:
#    cv2.imshow('okienko',face)
#    cv2.waitKey(0);


if url[:-4] == ".png":
    trans_mask = image[:, :, 3] == 0

    # replace areas of transparency with white and not transparent
    image[trans_mask] = [255, 255, 255, 255]

    # new image without alpha channel...
    image = cv2.cvtColor(image, cv2.COLOR_BGRA2BGR)

boxes = [[814,  495, 2078, 2162]]

for face in boxes:
    (x, y, w, h) = face
    print(x,y,w,h)
"""
