from app.errors import FaceRecognitionError
from app.face_recognition.IFaceRecognition import IFaceRecognition
import face_recognition
import numpy as np
from app.Constants import ROOT_DIR
import sys

from app.model.Result import Result

ftp_url = "ftp://156.17.128.143/"
http_url = "http://156.17.128.143/"


class FaceRecognition(IFaceRecognition):
    def __init__(self):

        self.cashed_faces = np.load(ROOT_DIR() + "\\face_encodings_similar_3.npy", allow_pickle=True)
        self.imges_paths = self.cashed_faces[:, 0]
        self.embeded_vec = np.vstack(self.cashed_faces[:, 1])

    def recognise_faces(self, img, number_of_results):
        result = []

        for image in img:
            try:
                encoding = face_recognition.face_encodings(image)[0]
                dista = face_recognition.face_distance(self.embeded_vec, encoding)
                # mapped_array = list(map(lambda x, y: (x, y), self.imges_paths, dist))
                # w tej linii do listy dodawane są już obiekty typu Result, dodatkowo distance jest zmieniany w procentowe podobienstwo
                # mapped_array = list(map(lambda x, y: Result(min((abs(1 - y) * 100) + 20, 100), x),  http_url + self.imges_paths, dist))
                # mapped_array = list(
                #     map(lambda x, y: Result(y, x), http_url + self.imges_paths, dist))
                ii = 0

                for dist in dista:
                        dist = -1 * dist
                        dist = dist + 0.2
                        if dist > -0.53:
                            dist = 1 / (1 + np.exp(-20 * (dist + 0.3)))
                        else:
                            dist = np.exp(dist - 4)

                        dista[ii] = dist * 100
                        ii=ii+1
                mapped_array = list(map(lambda x, y: Result(y, x), http_url + self.imges_paths, dista))

                # distance = sorted(mapped_array, key = itemgetter(1))
                #distance = sorted(mapped_array, key=lambda x: x.percentage, reverse=True)
                distance = sorted(mapped_array, key=lambda x: x.percentage, reverse=True)

                result.append(distance[:number_of_results])

            except:
                print(sys.exc_info()[0])
                print("Nie ma twarzy")
        return result
