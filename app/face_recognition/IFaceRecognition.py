from abc import ABC, abstractmethod


class IFaceRecognition(ABC):

    @abstractmethod
    def recognise_faces(self, faces , number_of_results):
        pass
