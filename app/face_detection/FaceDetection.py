import torch
from PIL import Image
from facenet_pytorch import MTCNN
from app.face_detection.IFaceDetection import IFaceDetection
import cv2
import numpy as np

class FaceDetection(IFaceDetection):
    def __init__(self, padding, image_size):
        self.device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
        self.mtcnn = MTCNN(keep_all=True, device=self.device)
        self.padding = padding
        self.image_size = image_size

    def detect_face(self, img):
        faces = []

        img = Image.fromarray(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
        boxes, _ = self.mtcnn.detect(img)

        for face in boxes:
            frame_draw = img.copy()
            frame_draw = frame_draw.crop(face)
            opencvImage = cv2.cvtColor(np.array(frame_draw), cv2.COLOR_RGB2BGR)

            # Resizing
            resized = cv2.resize(opencvImage, self.image_size, interpolation=cv2.INTER_LINEAR)
            # Normalizing
            norm_img = np.zeros(self.image_size)
            norm_img = cv2.normalize(resized, norm_img, 0, 255, cv2.NORM_MINMAX)
            faces.append(norm_img)

        return faces
