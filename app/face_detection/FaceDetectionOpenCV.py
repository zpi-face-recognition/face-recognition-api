import cv2
import numpy as np

from app.errors import NoFaceDetected
from app.face_detection.IFaceDetection import IFaceDetection
from numpy import asarray
import face_recognition
from PIL import Image


class FaceDetectionOpenCV(IFaceDetection):
    def __init__(self, padding, image_size):
        self.face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
        self.padding = padding
        self.image_size = image_size

    def detect_face(self, img):
        faces = []

        faces_detected = self.face_cascade.detectMultiScale(img, scaleFactor=1.1, minNeighbors=3)

        if len(faces_detected) == 0:
            raise NoFaceDetected

        for face in faces_detected:
            (x, y, w, h) = face

            # Cropping
            cropped = img[y - self.padding + 1:y + h + self.padding, x - self.padding + 1:x + w + self.padding]

            # Resizing
            resized = cv2.resize(cropped, self.image_size, interpolation=cv2.INTER_LINEAR)

            # Normalizing
            norm_img = np.zeros(self.image_size)
            norm_img = cv2.normalize(resized, norm_img, 0, 255, cv2.NORM_MINMAX)

            faces.append(norm_img)

        return faces
