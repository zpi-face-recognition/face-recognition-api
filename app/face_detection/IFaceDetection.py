from abc import ABC, abstractmethod


class IFaceDetection(ABC):

    @abstractmethod
    def detect_face(self, img):
        pass

